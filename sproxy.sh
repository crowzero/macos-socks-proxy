#!/bin/bash

# title:        sproxy.sh
# ------------------------
# description:  Opens an ssh connection. A Socks proxy is opened via this ssh connection.
# author:       Crowzero
# date:         20180329
# usage:        sproxy.sh start
# licens:       MIT https://opensource.org/licenses/MIT
VERSION="v1.0.0-beta.4"

# fill out with your data
ENTRYSERVER="user@your.server.com"

# the script starts
ARG=$1

# for developing:   get ENTRYSERVER from file
#                   no user@server will be commited to git
if [ -f "./entryserver.cfg" ]; then
    ENTRYSERVER=`cat ./entryserver.cfg`
fi

GETPIDCMD="ssh $ENTRYSERVER -D localhost:[0-9]\+ -f -N"
RUNNINGCOUNT=`ps -ef | grep "$GETPIDCMD" | grep -v "grep" | wc -l`
RUNNINGPORT=`ps -ef | grep "$GETPIDCMD" | grep -o "localhost:[0-9]\+" | grep -o "[0-9]\+"` # if Socks-proxy already running

NETWORKINTERFACE=""

started_as_sudo() {
    if [ `whoami` == "root" ]; then
        echo "ERRO: Don't start the script with sudo or as root. Script will stop know. Try again :-)"
        exit 1
    fi
}

# Prevantion for issue "Sporadic error at startup when SUDO password has to be entered"
# https://github.com/crowzero/macos-socks-proxy/issues/2
# The idea is to check if the password is already set for this session
# If not, we will ask for it
# If we now start the proxy, the password is already set
# ---
# Also this is one building block for the issue "Help issues for password retrieval"
# https://github.com/crowzero/macos-socks-proxy/issues/3
get_sudo_pw_for_this_session() {
    if ! sudo -n true 2>/dev/null; then # if sudo is not set for this session
        echo "INFO: To set the proxy on your Mac, we need sudo rights. Please type in your Mac password."
        if ! sudo echo >/dev/null; then # sudo will ask three times for the password - if the password is three times false, we will exit the script
            echo "ERRO: This is not your Mac password - we will stop now!"
            exit 1
        fi
    fi
}

get_active_network_interface(){
    services=$(networksetup -listnetworkserviceorder | grep 'Hardware Port')

    while read line; do
        sname=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $2}')
        sdev=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $4}')
        if [ -n "$sdev" ]; then
            ifout="$(ifconfig $sdev 2>/dev/null)"
            echo "$ifout" | grep 'status: active' > /dev/null 2>&1
            rc="$?"
            if [ "$rc" -eq 0 ]; then
                currentservice="$sname"
                currentdevice="$sdev"
            fi
        fi
    done <<< "$(echo "$services")"

    if [ -n "$currentservice" ]; then
        NETWORKINTERFACE=$currentservice
    else
        >&2 echo "Could not find current service"
        exit 1
    fi
}

get_free_port(){
    fromport=`sysctl net.inet.ip.portrange.first | cut -d " " -f 2`
    toport=`sysctl net.inet.ip.portrange.last | cut -d " " -f 2`
    # Get random free PROXYPORT
    PROXYPORT=`jot -r 1 $fromport $toport`
}

is_ssh_tunnel_running(){
    if [ $RUNNINGCOUNT == 0  ]; then
        return 1
    else
        return 0
    fi
}

is_proxy_running(){
    cmd=`networksetup -getsocksfirewallproxy "$NETWORKINTERFACE" -Fi | head -1 | cut -d ' ' -f2`
    if [ "$cmd" == "Yes" ]; then    # Proxy already running
        return 0
    elif [ "$cmd" == "No" ]; then   # Proxy isn't running
        return 1
    else                            # Error occured - stop all
        cmd=`networksetup -getsocksfirewallproxy "$NETWORKINTERFACE" -Fi | head -1 | grep -o "Error: .*" | sed 's/Error: //'`
        echo "ERRO: An error has occurred. The program ends!"
        echo "ERRO: $cmd"
        echo "ERRO: Network interface = <$NETWORKINTERFACE>"
        exit 1
    fi
}

test(){
    get_active_network_service
    soso=`networksetup -getsocksfirewallproxy "$NETWORKINTERFACE" -Fi | head -1 | cut -d ' ' -f2`
    echo $soso
}

display_version(){
    echo "$VERSION"
}

display_help(){
    echo ""
    echo "sproxy ($VERSION)"
    echo ""
    echo "NAME"
    echo -e "\tsproxy -- Socks Proxy over ssh"
    echo ""
    echo "SYNOPSIS"
    echo -e '\tsproxy [ start | stop | status | --version | --help | debug-help]'
    echo ""
    echo "DESCRIPTION"
    echo -e "\tOpens an ssh connection. A Socks proxy is opened via this ssh connection."
    echo ""
    echo -e "\tThe following options are available:"
    echo ""
    echo -e "\tstart                        Start starts the Socks-Proxy"
    echo ""
    echo -e "\tstop                         Stops the Socks-Proxy"
    echo ""
    echo -e "\tstatus                       Gives information about the status, i.e. if the ssh tunnel and the proxy are running."
    echo ""
    echo -e "\t-v, --version                Displys version number"
    echo ""
    echo -e "\tdebug-help                   Further commands that provide additional information for debugging"
    echo ""
    echo -e "\telse, --help                 Shows this help"
    echo ""
}

display_debug_help() {
    echo -e "\tFurther commands that provide additional information for debugging:"
    echo ""
    echo -e "\tnetwork                      Shows active networkservice"
    echo ""
    echo -e "\tport                         Shows proxy port"
    echo ""
    echo -e "\tisrunning                    Check if proxy is running"
    echo ""
    echo -e "\tunit_test_running_error      Simulates an error in function is_proxy_running()"
    echo ""
    echo -e "\ttest                         For testing and debugging shell commands"

}

stop_proxy(){
    get_sudo_pw_for_this_session
    if is_proxy_running; then
        echo "INFO: Stopping Proxy on interface <$NETWORKINTERFACE>"
        sudo networksetup -setsocksfirewallproxystate "$NETWORKINTERFACE" off
    else
        echo "WARN: No Proxy is running - nothing to stop"
    fi

    if is_ssh_tunnel_running; then
        echo "INFO: Stopping SSH Tunnel on port <$RUNNINGPORT> with pid:"
        for pid in `ps -ef | grep "$GETPIDCMD" | grep -v "grep" | awk '{print $2}'` ; do echo "<$pid>" ; kill $pid ; done
    else
        echo "WARN: No SSH Tunnel is running - nothing to stop"
    fi
}

start_proxy(){
    get_sudo_pw_for_this_session
    if ! is_ssh_tunnel_running; then
        get_free_port
        echo "INFO: Starting SSH Tunnel on port <$PROXYPORT>"
        CMD="ssh $ENTRYSERVER -D localhost:$PROXYPORT -f -N"
        $CMD &
    else
        echo "WARN: SSH Tunnel already running on port <$RUNNINGPORT> - wont start a new one"
        PROXYPORT="$RUNNINGPORT"
    fi

    if ! is_proxy_running; then
        echo "INFO: Starting Proxy on interface <$NETWORKINTERFACE>"
        sudo networksetup -setsocksfirewallproxy "$NETWORKINTERFACE" 127.0.0.1 "$PROXYPORT" off
    else
        echo "ERRO: Proxy is already running - will stop all"
        stop_proxy
    fi
}

started_as_sudo # Prevant scritp for started as sudo or root - exit
get_active_network_interface

case $ARG in
    "start")
        start_proxy
    ;;
    "stop")
        stop_proxy
    ;;
    "status")
        # SSH TUnnel
        if is_ssh_tunnel_running; then
            echo "INFO: SSH Tunnel is running on port $RUNNINGPORT with pid:"
            for pid in `ps -ef | grep "$GETPIDCMD" | grep -v "grep" | awk '{print $2}'` ; do echo $pid ; kill $pid ; done
        else
            echo "INFO: SSH Tunnel is not running"
        fi

        # Proxy
        if is_proxy_running; then
            echo "INFO: Proxy is running on network interface <$NETWORKINTERFACE>"
        else
            echo "INFO: No proxy is running on network interface <$NETWORKINTERFACE>"
        fi
    ;;
    "-v" | "--version")
        display_version
    ;;
    "network")
        get_active_network_service
    ;;
    "port")
        echo $RUNNINGPORT
    ;;
    "get-a-free-port")
        get_free_port
        echo $PROXYPORT
    ;;
    "unit_test_running_error")
        NETWORKINTERFACE="Blub"
        is_proxy_running
    ;;
    "test")
        test
    ;;
    "debug-help")
        display_help
        display_debug_help
    ;;
    *)
        display_help
    ;;
esac
