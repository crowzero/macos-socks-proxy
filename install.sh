#!/bin/bash

# title:        install.sh
# ------------------------
# description:  Copy script to /usr/local/bin/sproxy and set it to executable.
# author:       Crowzero
# date:         20180330
# usage:        install.sh
# licens:       MIT https://opensource.org/licenses/MIT

echo "Deploy sproxy - the macOS Socks Proxy"
sudo cp ./sproxy.sh /usr/local/bin/sproxy
sudo chmod 755 /usr/local/bin/sproxy
