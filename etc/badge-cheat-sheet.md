# Badge Cheat-Sheet
*   [Get Version](https://img.shields.io/badge/Version-v1.0.0--alpha.1-red.svg)
*   [Get Build](https://img.shields.io/badge/Build-passing-green.svg)
*   [Get Status](https://img.shields.io/badge/Status-stable-green.svg)
