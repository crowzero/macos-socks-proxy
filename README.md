<!-- FIXME: Create better hero image -->
![Hero](./etc/img/hero.jpg)

# macOS Socks Proxy

Opens a [SSH tunnel](https://en.wikipedia.org/wiki/Tunneling_protocol#Secure_Shell_tunneling) to a server and start a [Socks-Proxy](https://en.wikipedia.org/wiki/SOCKS) on the macOS. All network traffic will be redirected through this tunnel to the target SSH server.  
This offers the possibility to access all servers within the LAN network of the Target SSH server, depending on the security guidelines of this network. Similar to a VPN.  
The Mac also uses this network to access the Internet. This means that access to the Internet takes place with the IP address of the target network of the SSH server.  

## Getting Started

These instructions will get you a copy of **sproxy.sh** up and running on your local machine.

<!-- TODO: Descripe development with entryserver.cfg file -->
### Prerequisites
**Username and Host**
Open *sproxy.sh*. Fill in username@host of your ssh tunnel server.

```
# fill in with your data
ENTRYSERVER="user@your.server.com"
```

**SUDO**
To start the proxy you need sudo rights on your Mac.

### Installing

Just copy the script and make it executable.

```
# copy the script
./sproxy.sh /usr/local/bin/sproxy
# make it executable
chmod 755 /usr/local/bin/sproxy
```

#### Check out the help manual

```
sproxy --help
```

##### Result:

```

sproxy (v1.0.0-beta.4)

NAME
	sproxy -- Socks Proxy over ssh

SYNOPSIS
	sproxy [ start | stop | status | --version | --help | debug-help]

DESCRIPTION
	Opens an ssh connection. A Socks proxy is opened via this ssh connection.

	The following options are available:

	start                        Start starts the Socks-Proxy

	stop                         Stops the Socks-Proxy

	status                       Gives information about the status, i.e. if the ssh tunnel and the proxy are running.

	-v, --version                Displys version number

	debug-help                   Further commands that provide additional information for debugging

	else, --help                 Shows this help

```

## Issues
Please visit [https://github.com/crowzero/macos-socks-proxy/issues](https://github.com/crowzero/macos-socks-proxy/issues) to find all known issues. Feel free to open new issues.

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/crowzero/macos-socks-proxy/tags).

## Authors

*   **Crowzero** - *Initial work* - [Crowzero](https://github.com/crowzero)

See also the list of [contributors](./CONTRIBUTING.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

*   Thanx to [Reorx](https://apple.stackexchange.com/users/74719/reorx) for her/his inspiring answer at [How to find the currently connected network service from the command line?](https://apple.stackexchange.com/questions/191879/how-to-find-the-currently-connected-network-service-from-the-command-line#223446)
*   A big thanks goes also to [OttoAndras](https://github.com/OttoAndras) as patient sparring partner to find the errors.
*   Thanx to [PurpleBooth](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for the contributing template!
